import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './views/Home';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import AboutUs from './views/AboutUs';
import ContactUs from './views/ContactUs';
import Product from './views/Product';
import Cart from "./views/Cart";
import Checkout from "./views/Checkout";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" name="Home Page" component={Home} />
          <Route exact path="/aboutUs" name="About Us" component={AboutUs} />
          <Route exact path="/product" name="Product" component={Product} />
          <Route exact path="/cart" name="Cart" component={Cart} />
          <Route exact path="/checkout" name="Checkout" component={Checkout} />
          <Route exact path="/contactUs" name="Contact Us" component={ContactUs}/>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
