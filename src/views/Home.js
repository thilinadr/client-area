import React, {Component} from 'react';
import {Spinner} from 'reactstrap';
import Footer from '../containers/default-layout/Footer';
import Headers from '../containers/default-layout/Header';
import slider1 from '../assets/assets/images/slider/slider_1.jpg'
import slider2 from '../assets/assets/images/slider/slider_2.jpg'
import product7 from '../assets/assets/images/products/product-7.jpg'
import product8 from '../assets/assets/images/products/product-8.jpg'
import product9 from '../assets/assets/images/products/product-9.jpg'
import product10 from '../assets/assets/images/products/product-10.jpg'
import product11 from '../assets/assets/images/products/product-11.jpg'
import product12 from '../assets/assets/images/products/product-12.jpg'
import banner1 from '../assets/assets/images/banners/banner-1.jpg'
import banner2 from '../assets/assets/images/banners/banner-2.jpg'
import banner3 from '../assets/assets/images/banners/banner-3.jpg'
import banner4 from '../assets/assets/images/banners/banner-4.jpg'
import logo1 from '../assets/assets/images/logos/1.png'
import logo2 from '../assets/assets/images/logos/2.png'
import logo3 from '../assets/assets/images/logos/3.png'
import logo4 from '../assets/assets/images/logos/4.png'
import logo5 from '../assets/assets/images/logos/5.png'
import logo6 from '../assets/assets/images/logos/6.png'

import axios from 'axios';
import Cookies from 'js-cookie';

export default class Home extends Component {

    state = {
        categories: [],
        items: [],
        selectedItem: 'All Items',
        loading: false
    };

    getSubCatItems = (item) => {
        this.setState({
            selectedItem: item.name
        });
        // if (item.name === 'All Items') {
        //     this.getAllItems();
        // } else {
        //     axios.get('http://18.224.179.208/api/product_category/' + item.id)
        //         .then(res => {
        //             console.log(res.data);
        //             this.setState({
        //                 items: res.data
        //             })
        //         })
        //         .catch(error => {
        //             console.log(error)
        //         })
        // }
    };

    getAllItems = () => {
        axios.get('http://18.224.179.208/api/product')
            .then(res => {
                console.log(res.data);
                this.setState({
                    items: res.data
                })
            })
            .catch(error => {
                console.log(error)
            })
    };

    componentWillMount() {
        this.setState({
            loading: true
        });
        axios.get('http://18.224.179.208/api/category')
            .then(res => {
                console.log(res.data);
                this.setState({
                    categories: res.data[0].subCategories,
                    loading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    loading: false
                });
            });
        this.getAllItems();
    }

    render() {

        return (
            <div>
                <Headers history={this.props.history}/>
                <main className="main">

                    <div className="container mb-6">
                        <div
                            className="home-slider owl-carousel owl-carousel-lazy owl-theme nav-pos-outside show-nav-hover slide-animate">
                            <div className="marginTop home-slide banner banner-md-vw-small">
                                <img className="owl-lazy slide-bg" src={slider1} data-src={slider1} alt="banner"
                                     width="1120" height="445"/>
                                <div className="banner-layer slide-1 banner-layer-left banner-layer-middle text-right">
                                    <h4 className="m-b-3 text-right ">Luxury With Brands We Love</h4>
                                    <h3 className="m-b-2 font3 text-right text-primary "
                                        data-animation-name="blurIn" data-animation-duration="1200">Girl's Fashions</h3>

                                    <h4 className="m-b-3 text-right " data-animation-delay="700"
                                        data-animation-name="splitRight">Now at stock</h4>

                                    <div className="mb-0 " data-animation-delay="2400"
                                         data-animation-name="fadeInUpShorter">
                                        <a href="#items">
                                            <button className="btn btn-modern btn-lg btn-dark">Shop Now!</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="home-slide banner banner-md-vw-small">
                                <img className="owl-lazy slide-bg" src={slider2} data-src={slider2} alt="banner"
                                     width="1120" height="445"/>
                                <div className="banner-layer slide-2 banner-layer-right banner-layer-middle">
                                    <h4 className="m-b-3 ">Trending Items We Love</h4>
                                    <h3 className="m-b-2 font3 text-primary " data-animation-name="blurIn"
                                        data-animation-duration="1200">Boy's Fashions</h3>
                                    <h4 className="m-b-3 text-right " data-animation-delay="700"
                                        data-animation-name="splitRight">Now at stock</h4>
                                    <div className="mb-0 " data-animation-delay="2400"
                                         data-animation-name="fadeInUpShorter">
                                        <a href="#items">
                                            <button className="btn btn-modern btn-lg btn-dark">Shop Now!</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section>
                            <div className="container mt-3">
                                <div className="info-boxes-container border-bottom mb-3">
                                    <div className="row row-joined">
                                        <div className="feature-box col-sm-4 col-md-4 col-lg-4  text-center">
                                            <i className="icon-earphones-alt"></i>

                                            <div className="feature-box-content">
                                                <h3 className="mb-0 pb-1">Customer Support</h3>
                                                <h5>Need Assistence?</h5>

                                                <p>Contact us.</p>
                                            </div>
                                        </div>
                                        <div className="feature-box col-sm-4 col-md-4 col-lg-4  text-center">
                                            <i className="icon-credit-card"></i>

                                            <div className="feature-box-content">
                                                <h3 className="mb-0 pb-1">Secured Payment</h3>
                                                <h5>Safe &amp; Fast</h5>

                                                <p>Multiple payment types.</p>
                                            </div>
                                        </div>
                                        <div className="feature-box col-sm-4 col-md-4 col-lg-4  text-center">
                                            <i className="icon-action-undo "></i>

                                            <div className="feature-box-content">
                                                <h3 className="mb-0 pb-1">RETURNS</h3>
                                                <h5>Easy &amp; Free</h5>

                                                <p>Return if you not clear.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section className="p-t-3">
                            <h2 className="section-title ls-n-20 m-b-1 line-height-1 text-center">Products On Sale</h2>
                            <h3 className="section-sub-title ls-n-20 font-weight-normal m-b-4 text-center">All our sale products in a
                                exclusive brand selection</h3>

                            <div className="row offerItems mb-3">
                                {
                                    this.state.items.map((item, index) => {
                                        if (item.offer !== null) {
                                            return (
                                                <Item
                                                    key={index}
                                                    item={item}
                                                    onAdd={()=>this.setState({added:true})}
                                                    cookies={Cookies}
                                                    props={this.props}/>
                                            );
                                        }
                                    })
                                }
                            </div>

                        </section>

                        <section id='items'>
                            <div className="row">

                                <aside className="sidebar-home col-lg-3 col-md-4 order-lg-first">
                                    <div className="side-menu-wrapper m-b-3 border-0">
                                        <h2 className="side-menu-title bg-primary font-weight-sb text-white">
                                            <i className="icon-menu">

                                            </i>Shop By Category</h2>

                                        <nav className="side-nav border border-top-0">
                                            <ul className="menu menu-vertical sf-arrows">
                                                <li
                                                    className="catItem"
                                                    onClick={() => this.getSubCatItems({name: 'All Items'})}>
                                                    <a>All Items</a></li>
                                                {
                                                    this.state.categories.map((item, index) => {
                                                        return (
                                                            <li
                                                                className="catItem"
                                                                key={index}
                                                                onClick={() => this.getSubCatItems(item)}>
                                                                <a>{item.name}</a></li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </nav>
                                    </div>
                                </aside>
                                <div className="col-lg-9 col-md-8 m-b-3">
                                    <h2 className="section-title ls-n-20 m-b-1 line-height-1 text-center "
                                        data-animation-delay="100"
                                        data-animation-duration="1500">{this.state.selectedItem}</h2>
                                    <h3 className="section-sub-title ls-n-20 font-weight-normal m-b-4 text-center "
                                        data-animation-delay="100" data-animation-duration="1500">Find out items for
                                        your choice!</h3>
                                    <div className="row">
                                        {
                                            this.state.items.map((item, index) => {
                                                if (this.state.selectedItem === 'All Items') {
                                                    return (
                                                        <Item
                                                            key={index}
                                                            item={item}
                                                            onAdd={()=>this.setState({added:true})}
                                                            cookies={Cookies}
                                                            props={this.props}/>
                                                    );
                                                } else {
                                                    if (item.subCategory.name === this.state.selectedItem) {
                                                        return (
                                                            <Item
                                                                key={index}
                                                                item={item}
                                                                onAdd={()=>this.setState({added:true})}
                                                                cookies={Cookies}
                                                                props={this.props}/>
                                                        );
                                                    }
                                                }
                                            })
                                        }

                                    </div>
                                </div>
                            </div>
                        </section>

                        <section className="banner-container ">
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="banner">
                                        <img src={banner1} width="360" height="250" alt="banner"/>
                                        <div className="banner-layer banner-layer-right banner-layer-middle text-right">
                                            <h3 className="m-b-1 font3 text-right text-primary">Orange</h3>
                                            <h5 className="ls-n-20 d-inline-block m-r-2 text-left">FROM</h5>
                                            <h4 className="ls-n-20 text-price float-right text-left">$<b>19</b>99</h4>
                                            <div className="mb-0 clearfix text-right">
                                                <a href="category.html">
                                                    <button className="btn btn-modern btn-sm btn-dark">Shop Now!
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="banner">
                                        <img src={banner2} width="360" height="250" alt="banner"/>
                                        <div className="banner-layer banner-layer-right banner-layer-middle text-right">
                                            <h3 className="m-b-1 font3 text-right text-primary">White</h3>
                                            <h5 className="ls-n-20 d-inline-block m-r-2 text-left">FROM</h5>
                                            <h4 className="ls-n-20 text-price float-right text-left">$<b>29</b>99</h4>
                                            <div className="mb-0 clearfix text-right">
                                                <a href="category.html">
                                                    <button className="btn btn-modern btn-sm btn-dark">Shop Now!
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="banner">
                                        <img src={banner3} width="360" height="250" alt="banner"/>
                                        <div className="banner-layer banner-layer-right banner-layer-middle text-right">
                                            <h3 className="m-b-1 font3 text-right text-primary">Black</h3>
                                            <h5 className="ls-n-20 d-inline-block m-r-2 text-left">FROM</h5>
                                            <h4 className="ls-n-20 text-price float-right text-left">$<b>39</b>99</h4>
                                            <div className="mb-0 clearfix text-right">
                                                <a href="category.html">
                                                    <button className="btn btn-modern btn-sm btn-dark">Shop Now!
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section className="sale-banner m-t-3 ">
                            <div className="banner">
                                <img src={banner4} width="1120" height="380" alt="banner"/>
                                <div className="banner-layer banner-layer-middle banner-layer-left">
                                    <h5 className="font-weight-normal m-b-3 font3 text-left">Outlet Selected Items</h5>
                                    <h4 className="mb-0 text-left text-uppercase text-white">big sale up to</h4>
                                    <h3 className="text-sale text-left text-white mb-0">80%
                                        <small>OFF</small>
                                    </h3>
                                </div>
                            </div>
                        </section>

                        <div className="m-b-1"></div>

                    </div>
                </main>
                <Footer/>
            </div>
        );
    }
}


const Item = ({item, props,cookies,onAdd}) => {

    let price = item.price;
    if (item.offer !== null) {
        price = (item.price-(item.price/100*item.offer.percentage));
    }

    const addToCart = (i) => {
        let cart = cookies.get('CartItems');
        if (cart === undefined) {
            cart = [];
            cart.push({
                id: i.id,
                name: i.name,
                image: 'http://18.224.179.208/api/uploads/' + i.images[0].name,
                price: price,
                qty: 1
            })
        } else {
            cart = JSON.parse(cart);
            let cart2 = [];
            cart.map(item => {
                cart2.push(item.id);
            });
            if (!cart2.includes(i.id)) {
                cart.push({
                    id: i.id,
                    name: i.name,
                    image: 'http://18.224.179.208/api/uploads/' + i.images[0].name,
                    price: price,
                    qty: 1
                })
            }
        }
        cookies.set('CartItems',JSON.stringify(cart));
        onAdd();
    };

    return (
        <div className="col-6 col-md-3">
            <div className="product-default inner-quickview inner-icon"
                 data-animation-name="fadeInRightShorter">
                <figure>
                    <a onClick={() => props.history.push('/product', {item: item})}>
                        <img
                            src={'http://18.224.179.208/api/uploads/' + item.images[0].name}
                            alt="product" widht="400"
                            height="400"/>
                    </a>

                    <div className="label-group">
                        {
                            item.offer ?
                                <span
                                    className="product-label label-sale">-{item.offer.percentage}%</span>
                                : null
                        }
                    </div>
                    <div className="btn-icon-group">
                        <button
                            onClick={()=>addToCart(item)}
                            className="btn-icon btn-add-cart"
                                data-toggle="modal"
                                data-target="#addCartModal">
                            <i className="icon-bag"/>
                        </button>
                    </div>
                    {/*<a href="/Product.js"*/}
                       {/*className="btn-quickview" title="Quick View">Quick*/}
                        {/*View</a>*/}
                </figure>
                <div className="product-details">
                    <div className="category-wrap">
                        <div className="category-list">
                            <a
                               className="product-category">{item.subCategory.name}</a>
                        </div>
                        <a href="#" className="btn-icon-wish">
                            <i className="icon-heart"/></a>
                    </div>
                    <h2 className="product-title">
                        <a>{item.name}</a>
                    </h2>
                    <div className="ratings-container">
                        <div className="product-ratings">
                            <span className="ratings" style={{width: item.rating + '%'}}/>
                            <span className="tooltiptext tooltip-top"/>
                        </div>
                    </div>
                    <div className="price-box">

                    </div>

                    <div className="price-box">
                        {
                            item.offer ?
                                <>
                                    <span className="old-price">{item.price.toFixed(2)}</span>
                                    <span className="product-price">Rs. {(item.price-(item.price/100*item.offer.percentage))
                                        .toFixed(2)}</span>
                                </>
                                :
                                <span className="product-price">Rs. {item.price.toFixed(2)}</span>
                        }
                    </div>
                </div>
            </div>

        </div>
    )
};