import React, {Component} from 'react';
import Footer from '../containers/default-layout/Footer';
import Header from '../containers/default-layout/Header';

export default class AboutUs extends Component {
  render() {
    return (
      <div>
        <Header></Header>
        <main className="main">
          <div className="container mt-2 pt-0">
            <div
              className="outer-container page-header page-header-bg text-left"
              //   style="{{background : '70%/cover #D4E1EA url('assets/images/page-header-bg.jpg')'}}"
            >
              <div className="container pl-5">
                <h1>
                  <span>ABOUT US</span>
                  OUR COMPANY
                </h1>
                <a href="contact.html" className="btn btn-dark">
                  Contact
                </a>
              </div>
            </div>
          </div>

          <nav aria-label="breadcrumb" className="breadcrumb-nav">
            <div className="container">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="index.html">
                    <i className="icon-home"></i>
                  </a>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  About Us
                </li>
              </ol>
            </div>
          </nav>

          <div className="container pt-0">
            <div className="about-section">
              <h2 className="subtitle">OUR STORY</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged.
              </p>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>

              <p className="lead">
                “ Many desktop publishing packages and web page editors now use
                Lorem Ipsum as their default model search for evolved over
                sometimes by accident, sometimes on purpose ”
              </p>
            </div>
          </div>

          <div className="container pt-0">
            <div className="features-section">
              <h2 className="subtitle">WHY CHOOSE US</h2>

              <div className="row">
                <div className="col-lg-4">
                  <div className="feature-box bg-white">
                    <i className="icon-shipped"></i>

                    <div className="feature-box-content">
                      <h3>Free Shipping</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the industr
                        in some form.
                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="feature-box bg-white">
                    <i className="icon-us-dollar"></i>

                    <div className="feature-box-content">
                      <h3>100% Money Back Guarantee</h3>
                      <p>
                        There are many variations of passages of Lorem Ipsum
                        available, but the majority have suffered alteration in
                        some form.
                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="feature-box bg-white">
                    <i className="icon-online-support"></i>

                    <div className="feature-box-content">
                      <h3>Online Support 24/7</h3>
                      <p>
                        There are many variations of passages of Lorem Ipsum
                        available, but the majority have suffered alteration in
                        some form.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container pt-0">
            <div className="testimonials-section">
              <h2 className="subtitle text-center">HAPPY CLIENTS</h2>

              <div
                className="testimonials-carousel owl-carousel owl-theme images-left"
                data-toggle="owl"
                data-owl-options="{
						'lazyLoad': true,
						'autoHeight': true,
						'dots': false,
						'responsive': {
							'0': {
								'items': 1
							},
							'992': {
								'items': 2
							}
						}
					}"
              >
                <div className="testimonial">
                  <div className="testimonial-owner">
                    <figure>
                      <img
                        src={require("../assets/assets/images/clients/client1.png")}
                        alt="client"
                      />
                    </figure>

                    <div>
                      <h4 className="testimonial-title">john Smith</h4>
                      <span>Proto Co Ceo</span>
                    </div>
                  </div>

                  <blockquote>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur elitad adipiscing
                      Cras non placerat mipsum dolor sit amet, consectetur
                      elitad adipiscing.
                    </p>
                  </blockquote>
                </div>

                <div className="testimonial">
                  <div className="testimonial-owner">
                    <figure>
                      <img
                        src={require("../assets/assets/images/clients/client2.png")}
                        alt="client"
                      />
                    </figure>

                    <div>
                      <h4 className="testimonial-title">Bob Smith</h4>
                      <span>Proto Co Ceo</span>
                    </div>
                  </div>

                  <blockquote>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur elitad adipiscing
                      Cras non placerat mipsum dolor sit amet, consectetur
                      elitad adipiscing.
                    </p>
                  </blockquote>
                </div>

                <div className="testimonial">
                  <div className="testimonial-owner">
                    <figure>
                      <img
                        src={require("../assets/assets/images/clients/client1.png")}
                        alt="client"
                      />
                    </figure>

                    <div>
                      <h4 className="testimonial-title">john Smith</h4>
                      <span>Proto Co Ceo</span>
                    </div>
                  </div>

                  <blockquote>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur elitad adipiscing
                      Cras non placerat mipsum dolor sit amet, consectetur
                      elitad adipiscing.
                    </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>

          <div className="container pt-0">
            <div className="counters-section">
              <div className="row">
                <div className="col-6 col-md-4 count-container">
                  <div className="count-wrapper">
                    <span
                      className="count-to"
                      data-from="0"
                      data-to="200"
                      data-speed="2000"
                      data-refresh-interval="50"
                    >
                      200
                    </span>
                    +
                  </div>
                  <h4 className="count-title">MILLION CUSTOMERS</h4>
                </div>

                <div className="col-6 col-md-4 count-container">
                  <div className="count-wrapper">
                    <span
                      className="count-to"
                      data-from="0"
                      data-to="1800"
                      data-speed="2000"
                      data-refresh-interval="50"
                    >
                      1800
                    </span>
                    +
                  </div>
                  <h4 className="count-title">TEAM MEMBERS</h4>
                </div>

                <div className="col-6 col-md-4 count-container">
                  <div className="count-wrapper">
                    <span
                      className="count-to"
                      data-from="0"
                      data-to="24"
                      data-speed="2000"
                      data-refresh-interval="50"
                    >
                      24
                    </span>
                    <span>HR</span>
                  </div>
                  <h4 className="count-title">SUPPORT AVAILABLE</h4>
                </div>

                <div className="col-6 col-md-4 count-container">
                  <div className="count-wrapper">
                    <span
                      className="count-to"
                      data-from="0"
                      data-to="265"
                      data-speed="2000"
                      data-refresh-interval="50"
                    >
                      265
                    </span>
                    +
                  </div>
                  <h4 className="count-title">SUPPORT AVAILABLE</h4>
                </div>

                <div className="col-6 col-md-4 count-container">
                  <div className="count-wrapper">
                    <span
                      className="count-to"
                      data-from="0"
                      data-to="99"
                      data-speed="2000"
                      data-refresh-interval="50"
                    >
                      99
                    </span>
                    <span>%</span>
                  </div>
                  <h4 className="count-title">SUPPORT AVAILABLE</h4>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer></Footer>
      </div>
    );
  }
}
