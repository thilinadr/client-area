import React, {Component} from 'react';
import Header from '../containers/default-layout/Header';
import Footer from '../containers/default-layout/Footer';

export default class ContactUs extends Component {
  render() {
    return (
      <div>
        <Header></Header>
        <main className="main">
          <nav aria-label="breadcrumb" className="breadcrumb-nav">
            <div className="container">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="index.html">
                    <i className="icon-home"></i>
                  </a>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  Contact Us
                </li>
              </ol>
            </div>
          </nav>

          <div className="container mb-8">
            <div id="map"></div>

            <div className="row">
              <div className="col-md-8">
                <h2 className="light-title">
                  Write <strong>Us</strong>
                </h2>

                <form action="#">
                  <div className="form-group required-field">
                    <label for="contact-name">Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="contact-name"
                      name="contact-name"
                      required
                    />
                  </div>

                  <div className="form-group required-field">
                    <label for="contact-email">Email</label>
                    <input
                      type="email"
                      className="form-control"
                      id="contact-email"
                      name="contact-email"
                      required
                    />
                  </div>

                  <div className="form-group">
                    <label for="contact-phone">Phone Number</label>
                    <input
                      type="tel"
                      className="form-control"
                      id="contact-phone"
                      name="contact-phone"
                    />
                  </div>

                  <div className="form-group required-field">
                    <label for="contact-message">What’s on your mind?</label>
                    <textarea
                      cols="30"
                      rows="1"
                      id="contact-message"
                      className="form-control"
                      name="contact-message"
                      required
                    ></textarea>
                  </div>

                  <div className="form-footer">
                    <button type="submit" className="btn btn-primary">
                      Submit
                    </button>
                  </div>
                </form>
              </div>

              <div className="col-md-4">
                <h2 className="light-title">
                  Contact <strong>Details</strong>
                </h2>

                <div className="contact-info">
                  <div>
                    <i className="icon-phone"></i>
                    <p>
                      <a href="tel:">0201 203 2032</a>
                    </p>
                    <p>
                      <a href="tel:">0201 203 2032</a>
                    </p>
                  </div>
                  <div>
                    <i className="icon-mobile"></i>
                    <p>
                      <a href="tel:">201-123-3922</a>
                    </p>
                    <p>
                      <a href="tel:">302-123-3928</a>
                    </p>
                  </div>
                  <div>
                    <i className="icon-mail-alt"></i>
                    <p>
                      <a href="mailto:#">porto@gmail.com</a>
                    </p>
                    <p>
                      <a href="mailto:#">porto@portotemplate.com</a>
                    </p>
                  </div>
                  <div>
                    <i className="icon-skype"></i>
                    <p>porto_skype</p>
                    <p>porto_template</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer></Footer>
      </div>
    );
  }
}
