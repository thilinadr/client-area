import React from 'react';
import Headers from '../containers/default-layout/Header';
import Cookies from "js-cookie";

class Checkout extends React.Component {
    render() {
        let cart = Cookies.get('CartItems');
        if (cart === undefined) {
            cart = [];
        } else {
            cart = JSON.parse(cart);
        }
        return(
            <div>
                <Headers history={this.props.history}/>
                <main className="main">
                    <nav aria-label="breadcrumb" className="breadcrumb-nav">
                        <div className="container">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><a onClick={() => this.props.history.push('/')}><i
                                    className="icon-home"/></a>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">Checkout</li>
                            </ol>

                            <div className="container mb-6">
                                <ul className="checkout-progress-bar">
                                    <li className="active">
                                        <span>Shipping</span>
                                    </li>
                                    <li>
                                        <span>Review &amp; Payments</span>
                                    </li>
                                </ul>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <ul className="checkout-steps">
                                            <li>
                                                <h2 className="step-title">Login</h2>

                                                <p>If you already have an account with us. Sign in or Join with us.</p>

                                                <form action="#">
                                                    <div className="form-group required-field">
                                                        <label>Email Address </label>
                                                        <input type="email" className="form-control" required=""/>
                                                    </div>

                                                    <div className="form-group required-field">
                                                        <label>Password </label>
                                                        <input type="password" className="form-control" required=""/>
                                                    </div>

                                                    <div className="form-footer">
                                                        <button style={{height: 50,borderRadius:50}} type="submit" className="btn btn-primary">LOGIN</button>
                                                    </div>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-6">
                                        <ul className="checkout-steps">
                                            <li>
                                                <h2 className="step-title">Register</h2>

                                                <p>Join with us.</p>

                                                <form action="#">
                                                    <div className="form-group required-field">
                                                        <label>Full Name </label>
                                                        <input type="text" className="form-control" required=""/>
                                                    </div>

                                                    <div className="form-group">
                                                        <label>Password </label>
                                                        <input type="password" className="form-control" required=""/>
                                                    </div>

                                                    <div className="form-group required-field">
                                                        <label>Address </label>
                                                        <input type="text" className="form-control" required=""/>
                                                    </div>

                                                    <div className="form-group required-field">
                                                        <label>Phone Number </label>
                                                        <input type="tel" className="form-control" required=""/>
                                                    </div>

                                                    <div className="form-group required-field">
                                                        <label>Email Address </label>
                                                        <input type="email" className="form-control" required=""/>
                                                    </div>

                                                    <div className="form-footer">
                                                        <button style={{height: 50,borderRadius:50}} type="submit"
                                                                className="btn btn-primary">REGISTER</button>
                                                    </div>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </nav>
                </main>
            </div>
        )
    }
}

export default Checkout;