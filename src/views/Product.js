import React, {Component} from 'react';
import axios from "axios/index";
import {Spinner} from "reactstrap";
import Headers from '../containers/default-layout/Header';
import Cookies from 'js-cookie';

class Product extends Component {

    state = {
        loading: true,
        item: {},
        qty: this.props.location.state.item.qty ?? 1
    };

    componentWillMount() {
        axios.get('http://18.224.179.208/api/product/' + this.props.location.state.item.id)
            .then(res => {
                console.log(res.data);
                this.setState({
                    item: res.data,
                    loading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    loading: false
                });
            });
    }

    addToCart = (i) => {
        let price = i.price;
        if (i.offer !== null) {
            price = (i.price-(i.price/100*i.offer.percentage));
        }
        let cart = Cookies.get('CartItems');
        if (cart === undefined) {
            cart = [];
            cart.push({
                id: i.id,
                name: i.name,
                image: 'http://18.224.179.208/api/uploads/' + i.images[0].name,
                price: price,
                qty: this.state.qty
            })
        } else {
            cart = JSON.parse(cart);
            let cart2 = [];
            console.log('qty',this.state.qty);
            cart.map(item => {
                if (item.id === i.id) {
                    cart2.push({
                        id: i.id,
                        name: i.name,
                        image: 'http://18.224.179.208/api/uploads/' + i.images[0].name,
                        price: price,
                        qty: this.state.qty
                    })
                } else {
                    cart2.push(item);
                }
            });
            cart = cart2;
        }
        Cookies.set('CartItems',JSON.stringify(cart));
        this.setState({
            loading: false
        });
        this.props.history.push('./cart');
    };

    render() {
        return (
            <div>
                <Headers history={this.props.history}/>
                <main className="main">

                    <nav aria-label="breadcrumb" className="breadcrumb-nav">
                        <div className="container">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><a onClick={()=>this.props.history.push('/')}><i className="icon-home"/></a>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">Product Item</li>
                            </ol>
                        </div>
                    </nav>

                    <div className="container mb-6">
                        <div className="product-single-container product-single-default product-quick-view">
                            {
                                this.state.loading ?
                                    <Spinner size={'small'} color={'black'}/> :
                                    <div className="row row-sparse">
                                        <div className="marginTop col-lg-6 product-single-gallery">
                                            <div className="product-slider-container">
                                                <div className="product-single-carousel owl-theme">
                                                    <img
                                                        src={'http://18.224.179.208/api/uploads/' + this.state.item.images[0].name}
                                                        alt="product" widht="300"
                                                        height="300"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-6 product-single-details">
                                            <h1 className="product-title">{this.state.item.name}</h1>

                                            <div className="ratings-container">
                                                <div className="product-ratings">
                                                    <span className="ratings" style={{width: this.state.item.rating + '%'}}/>
                                                </div>

                                                <a href="#" className="rating-link">( 0 Reviews )</a>
                                            </div>

                                            <div className="price-box">
                                                {
                                                    this.state.item.offer ?
                                                        <>
                                                            <span className="old-price">Rs. {this.state.item.price.toFixed(2)}</span>
                                                            <span className="product-price">Rs. {(this.state.item.price-
                                                                (this.state.item.price/100*this.state.item.offer.percentage))
                                                                .toFixed(2)}</span>
                                                        </>
                                                        :
                                                        <span className="product-price">Rs. {this.state.item.price.toFixed(2)}</span>
                                                }
                                            </div>

                                            <div className="product-desc">
                                                <p>{this.state.item.description}</p>
                                            </div>

                                            <div className="product-filters-container">
                                                <div className="product-single-filter">
                                                    <label>Sizes:</label>
                                                    {
                                                        this.state.item.productsizes.map((item,index)=>(
                                                            <label key={index}>{item.name}</label>
                                                        ))
                                                    }
                                                </div>
                                            </div>

                                            <hr className="divider"/>

                                            <div className="product-action">
                                                <div className="product-single-qty">
                                                    <input
                                                        value={this.state.qty}
                                                        onChange={e => {
                                                            if (e.target.value > 0)
                                                                this.setState({qty: e.target.value})
                                                        }}
                                                        className="horizontal-quantity form-control"
                                                        style={{marginBottom: 0}}
                                                        type="number"/>
                                                </div>

                                                <a onClick={()=>this.addToCart(this.state.item)}
                                                   className="btn btn-dark add-cart" title="Add to Cart">Add to Cart</a>
                                            </div>

                                            <hr className="divider"/>

                                            <div className="product-single-share">
                                                <a href="#" className="add-wishlist" title="Add to Wishlist">Add to Wishlist</a>
                                            </div>
                                        </div>
                                    </div>
                            }

                        </div>

                        <div className="product-single-tabs">
                            <ul className="nav nav-tabs" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" id="product-tab-desc" data-toggle="tab"
                                       href="#product-desc-content" role="tab" aria-controls="product-desc-content"
                                       aria-selected="true">Description</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="product-tab-reviews" data-toggle="tab"
                                       href="#product-reviews-content" role="tab"
                                       aria-controls="product-reviews-content" aria-selected="false">Reviews (3)</a>
                                </li>
                            </ul>
                            <div className="tab-content">
                                <div className="tab-pane fade show active" id="product-desc-content" role="tabpanel"
                                     aria-labelledby="product-tab-desc">
                                    <div className="product-desc-content">
                                        <p>{this.state.item.description}</p>
                                    </div>
                                </div>

                                <div className="tab-pane fade" id="product-reviews-content" role="tabpanel"
                                     aria-labelledby="product-tab-reviews">
                                    <div className="product-reviews-content">
                                        <div className="row">
                                            <div className="col-xl-7">
                                                <h2 className="reviews-title">3 reviews for Product Long Name</h2>

                                                <ol className="comment-list">
                                                    <li className="comment-container">
                                                        <div className="comment-avatar">
                                                            <img src="../assets/assets/images/avatar/avatar1.jpg" width="65"
                                                                 height="65" alt="avatar"/>
                                                        </div>

                                                        <div className="comment-box">
                                                            <div className="ratings-container">
                                                                <div className="product-ratings">
                                                                    <span className="ratings" style={{width: '80%'}}/>
                                                                </div>
                                                            </div>

                                                            <div className="comment-info mb-1">
                                                                <h4 className="avatar-name">John Doe</h4> - <span
                                                                className="comment-date">Novemeber 15, 2019</span>
                                                            </div>

                                                            <div className="comment-text">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                                    elit, sed do eiusmod tempor incididunt ut labore et
                                                                    dolore magna aliqua. Ut enim ad minim veniam, quis
                                                                    nostrud exercitation ullamco laboris nisi ut
                                                                    aliquip.</p>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li className="comment-container">
                                                        <div className="comment-avatar">
                                                            <img src="../assets/assets/images/avatar/avatar2.jpg" width="65"
                                                                 height="65" alt="avatar"/>
                                                        </div>

                                                        <div className="comment-box">
                                                            <div className="ratings-container">
                                                                <div className="product-ratings">
                                                                    <span className="ratings" style={{width: '80%'}}/>
                                                                </div>
                                                            </div>

                                                            <div className="comment-info mb-1">
                                                                <h4 className="avatar-name">John Doe</h4> - <span
                                                                className="comment-date">Novemeber 15, 2019</span>
                                                            </div>

                                                            <div className="comment-text">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                                    elit, sed do eiusmod tempor incididunt ut labore et
                                                                    dolore magna aliqua. Ut enim ad minim veniam, quis
                                                                    nostrud exercitation ullamco laboris nisi ut
                                                                    aliquip.</p>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li className="comment-container">
                                                        <div className="comment-avatar">
                                                            <img src="../assets/assets/images/avatar/avatar3.jpg" width="65"
                                                                 height="65" alt="avatar"/>
                                                        </div>

                                                        <div className="comment-box">
                                                            <div className="ratings-container">
                                                                <div className="product-ratings">
                                                                    <span className="ratings" style={{width: '80%'}}/>
                                                                </div>
                                                            </div>

                                                            <div className="comment-info mb-1">
                                                                <h4 className="avatar-name">John Doe</h4> - <span
                                                                className="comment-date">Novemeber 15, 2019</span>
                                                            </div>

                                                            <div className="comment-text">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                                    elit, sed do eiusmod tempor incididunt ut labore et
                                                                    dolore magna aliqua. Ut enim ad minim veniam, quis
                                                                    nostrud exercitation ullamco laboris nisi ut
                                                                    aliquip.</p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ol>
                                            </div>

                                            <div className="col-xl-5">
                                                <div className="add-product-review">
                                                    <form action="#" className="comment-form m-0">
                                                        <h3 className="review-title">Add a Review</h3>

                                                        <div className="rating-form">
                                                            <label htmlFor="rating">Your rating</label>
                                                            <span className="rating-stars">
														<a className="star-1" href="#">1</a>
														<a className="star-2" href="#">2</a>
														<a className="star-3" href="#">3</a>
														<a className="star-4" href="#">4</a>
														<a className="star-5" href="#">5</a>
													</span>

                                                            <select name="rating" id="rating" required=""
                                                                    style={{display: 'none'}}>
                                                                <option value="">Rate…</option>
                                                                <option value="5">Perfect</option>
                                                                <option value="4">Good</option>
                                                                <option value="3">Average</option>
                                                                <option value="2">Not that bad</option>
                                                                <option value="1">Very poor</option>
                                                            </select>
                                                        </div>

                                                        <div className="form-group">
                                                            <label>Your Review</label>
                                                            <textarea cols="5" rows="6"
                                                                      className="form-control form-control-sm"/>
                                                        </div>

                                                        <div className="row">
                                                            <div className="col-md-6 col-xl-12">
                                                                <div className="form-group">
                                                                    <label>Your Name</label>
                                                                    <input type="text"
                                                                           className="form-control form-control-sm"
                                                                           required=""/>
                                                                </div>
                                                            </div>

                                                            <div className="col-md-6 col-xl-12">
                                                                <div className="form-group">
                                                                    <label>Your E-mail</label>
                                                                    <input type="text"
                                                                           className="form-control form-control-sm"
                                                                           required=""/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <input type="submit" className="btn btn-dark ls-n-15"
                                                               value="Submit"/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </main>
            </div>
        )
    }
}

export default Product