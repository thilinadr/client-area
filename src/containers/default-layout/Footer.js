import React, {Component} from 'react';
import logoWhite from '../../assets/assets/images/logo-white.png'

export default class Footer extends Component {
  render() {
    return (
      <div>
        <footer className="footer bg-dark">
          <div className="container">
            <div className="footer-top">
              <div className="row row-sm">
                <div className="col-lg-5">
                  <div className="widget widget-about">
                    <h4 className="widget-title">About Us</h4>
                    <a href="index.html" className="logo mb-2">
                      <img
                        src={logoWhite}
                        className="m-b-3"
                        width="110"
                        height="46"
                        alt="Porto Logo"
                      />
                    </a>
                    <p className="mb-3">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Duis nec vestibulum magna, et dapibus lacus.{' '}
                      <a href="#" className="text-white">
                        <strong>
                          <ins>read more...</ins>
                        </strong>
                      </a>
                    </p>
                  </div>

                  <div>
                    <div className="widget">
                      <h4 className="widget-title">Contact Info</h4>
                      <ul className="contact-info d-flex flex-wrap">
                        <li>
                          <span className="contact-info-label">Address:</span>
                          1234 Street Name, City, Country
                        </li>
                        <li>
                          <span className="contact-info-label">Phone:</span>Toll
                          Free <a href="tel:">(123) 456-7890</a>
                        </li>
                        <li>
                          <span className="contact-info-label">Email:</span>{' '}
                          <a href="mailto:mail@example.com">mail@example.com</a>
                        </li>
                        <li>
                          <span className="contact-info-label">
                            Working Days/Hours:
                          </span>
                          <a href="tel:">Mon - Sun / 9:00 AM - 8:00 PM</a>
                        </li>
                      </ul>
                    </div>
                    <div className="social-icons">
                      <a
                        href="#"
                        className="social-icon social-facebook icon-facebook"
                        target="_blank"
                        title="Facebook"
                      ></a>
                      <a
                        href="#"
                        className="social-icon social-twitter icon-twitter"
                        target="_blank"
                        title="Twitter"
                      ></a>
                      <a
                        href="#"
                        className="social-icon social-instagram icon-instagram"
                        target="_blank"
                        title="Instagram"
                      ></a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="widget">
                    <h4 className="widget-title">Featured Products</h4>
                    <div className="product-default left-details product-widget">
                      <figure>
                        <a href="product.html">
                          <img
                            src={require("../../assets/assets/images/products/product-7.jpg")}
                            width="95"
                            height="95"
                          />
                        </a>
                      </figure>
                      <div className="product-details">
                        <h2 className="product-title text-white">
                          <a href="product.html">Woman Black Blouse</a>
                        </h2>
                        <div className="ratings-container">
                          <div className="product-ratings">
                            {/* <span className="ratings" style="width:100%"></span> */}
                            <span className="tooltiptext tooltip-top"></span>
                          </div>
                        </div>
                        <div className="price-box">
                          <span className="product-price">$129.00</span>
                        </div>
                      </div>
                    </div>
                    <div className="product-default left-details product-widget">
                      <figure>
                        <a href="product.html">
                          <img
                            src={require("../../assets/assets/images/products/product-4.jpg")}
                            width="95"
                            height="95"
                          />
                        </a>
                      </figure>
                      <div className="product-details">
                        <h2 className="product-title text-white">
                          <a href="product.html">Jeans Wear</a>
                        </h2>
                        <div className="ratings-container">
                          <div className="product-ratings">
                            {/* <span className="ratings" style="width:100%"></span> */}
                            <span className="tooltiptext tooltip-top"></span>
                          </div>
                        </div>
                        <div className="price-box">
                          <span className="product-price">$185.00</span>
                        </div>
                      </div>
                    </div>
                    <div className="product-default left-details product-widget">
                      <figure>
                        <a href="product.html">
                          <img
                            src={require("../../assets/assets/images/products/product-3.jpg")}
                            width="95"
                            height="95"
                          />
                        </a>
                      </figure>
                      <div className="product-details">
                        <h2 className="product-title text-white">
                          <a href="product.html">Porto Sticky Info</a>
                        </h2>
                        <div className="ratings-container">
                          <div className="product-ratings">
                            {/* <span className="ratings" style="width:100%"></span> */}
                            <span className="tooltiptext tooltip-top"></span>
                          </div>
                        </div>
                        <div className="price-box">
                          <span className="product-price">$129.00</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="widget">
                    <h4 className="widget-title">Twitter Feeds</h4>

                    <div className="twitter-feed">
                      <div className="twitter-feed-content">
                        <a
                          className="d-inline-block align-top text-white"
                          href="#"
                        >
                          <i className="mr-2 icon-twitter"></i>
                        </a>
                        <p className="d-inline-block">
                          Oops, our twitter feed is unavailable right now.
                          <br />
                          <a className="meta" href="http://twitter.com/swtheme">
                            Follow us on Twitter
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="footer-middle">
              <div className="row">
                <div className="col-lg-5 mb-4 mb-lg-0">
                  <div className="widget">
                    <h4 className="widget-title">Customer Service</h4>

                    <ul className="links link-parts row mb-0">
                      <div className="link-part col-md-6 col-sm-12">
                        <li>
                          <a href="about.html">About us</a>
                        </li>
                        <li>
                          <a href="contact.html">Contact us</a>
                        </li>
                        <li>
                          <a href="#">My Account</a>
                        </li>
                      </div>
                      <div className="link-part col-md-6 col-sm-12">
                        <li>
                          <a href="#">Order history</a>
                        </li>
                        <li>
                          <a href="#">Advanced search</a>
                        </li>
                        <li>
                          <a href="#" className="login-link">
                            Login
                          </a>
                        </li>
                      </div>
                    </ul>
                  </div>
                </div>

                <div className="col-lg-7">
                  <div className="widget">
                    <h4 className="widget-title">Main Features</h4>

                    <ul className="links link-parts row mb-0">
                      <div className="link-part col-md-6 col-sm-12">
                        <li>
                          <a href="#">Super Fast Wordpress Theme</a>
                        </li>
                        <li>
                          <a href="#">1st Fully working Ajax Theme</a>
                        </li>
                        <li>
                          <a href="#">34 Unique Shop Layouts</a>
                        </li>
                      </div>
                      <div className="link-part col-md-6 col-sm-12">
                        <li>
                          <a href="#">Powerful Admin Panel</a>
                        </li>
                        <li>
                          <a href="#">Mobile & Retina Optimized</a>
                        </li>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
              <p className="footer-copyright py-3 pr-4 mb-0">
                &copy; Porto eCommerce. 2020. All Rights Reserved
              </p>

              <img
                src={require("../../assets/assets/images/payments.png")}
                alt="payment"
                width="240"
                height="52"
                className="footer-payments py-3"
              />
            </div>
          </div>
        </footer>
      </div>
    );
  }
}
